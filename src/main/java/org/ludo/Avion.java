package org.ludo;

public class Avion {
// Déclaration des variables
    private String typeAvion ;
    private String modeleAvion;
    private Integer nbPassager;
    private String codeAvion;


// GETTER
    public String getTypeAvion() {
        return typeAvion;
    }
    public String getModeleAvion() {
        return modeleAvion;
    }
    public Integer getNbPassager() {
        return nbPassager;
    }
    public String getCodeAvion() {
        return codeAvion;
    }
// SETTER
    public void setTypeAvion(String typeAvion) {
        this.typeAvion = typeAvion;
    }
    public void setModeleAvion(String modeleAvion) {
        this.modeleAvion = modeleAvion;
    }
    public void setNbPassager(Integer nbPassager) {
        this.nbPassager = nbPassager;
    }
    public void setCodeAvion(String codeAvion) {
        this.codeAvion = codeAvion;
    }

    public Avion(String typeAvion, String modeleAvion, Integer nbPassager, String codeAvion) {
        this.typeAvion = typeAvion;
        this.modeleAvion = modeleAvion;
        this.nbPassager = nbPassager;
        this.codeAvion = codeAvion;
    }
}
