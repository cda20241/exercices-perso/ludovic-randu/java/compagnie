package org.ludo;

public class Vol {
    // Déclaration des variables
    private Integer numVol;
    private String dateDepart;
    private String dateArrivee;
    private String heureDepart;
    private String heureArrivee;
    private String villeDepart;
    private String villeArrivee;
    private Integer retard;
    private Avion avion;





    // GETTER
    public Integer getNumVol() {
        return numVol;
    }
    public String getDateDepart() {
        return dateDepart;
    }
    public String getDateArrivee() {
        return dateArrivee;
    }
    public String getHeureDepart() {
        return heureDepart;
    }
    public String getHeureArrivee() {
        return heureArrivee;
    }
    public String getVilleDepart() {
        return villeDepart;
    }
    public String getVilleArrivee() {
        return villeArrivee;
    }
    public Integer getRetard() {
        return retard;
    }
// SETTER
    public void setNumVol(Integer numVol) {
        this.numVol = numVol;
    }
    public void setDateDepart(String dateDepart) {
        this.dateDepart = dateDepart;
    }
    public void setDateArrivee(String dateArrivee) {
        this.dateArrivee = dateArrivee;
    }
    public void setHeureDepart(String heureDepart) {
        this.heureDepart = heureDepart;
    }
    public void setHeureArrivee(String heureArrivee) {
        this.heureArrivee = heureArrivee;
    }
    public void setVilleDepart(String villeDepart) {
        this.villeDepart = villeDepart;
    }
    public void setVilleArrivee(String villeArrivee) {
        this.villeArrivee = villeArrivee;
    }
    public void setRetard(Integer retard) {
        this.retard = retard;
    }



    public Vol(Integer numVol, String dateDepart, String dateArrivee, String heureDepart, String heureArrivee, String villeDepart, String villeArrivee) {
        this.numVol = numVol;
        this.dateDepart = dateDepart;
        this.dateArrivee = dateArrivee;
        this.heureDepart = heureDepart;
        this.heureArrivee = heureArrivee;
        this.villeDepart = villeDepart;
        this.villeArrivee = villeArrivee;
    }

}
