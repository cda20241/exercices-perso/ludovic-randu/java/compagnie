package org.ludo;

public class Siege {
    private int allee;
    private int numRang;
    private int classe;
    private Billet billet;

    public Siege(int allee, int numRang, int classe) {
        this.allee = allee;
        this.numRang = numRang;
        this.classe = classe;
    }
}
