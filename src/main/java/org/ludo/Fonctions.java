package org.ludo;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Fonctions {

    public static Integer genNumBillet() {
        return (int) (Math.random() * (10000 + 99999));
    }

    public static String nouvelleDate(){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("uuuu/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        return dtf.format(now);
    }
}
