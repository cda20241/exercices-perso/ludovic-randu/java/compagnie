package org.ludo;

import java.util.ArrayList;
import java.util.List;

public class Pilote {
    private Integer matricule;
    private String nom;
    private String prenom;
    private String qualif;
    private Compagnie compagnie;
// GETTER
    public Integer getMatricule() {
        return matricule;
    }
    public String getNom() {
        return nom;
    }
    public String getPrenom() {
        return prenom;
    }
    public String getQualif() {
        return qualif;
    }

//SETTER
    public void setMatricule(Integer matricule) {
        this.matricule = matricule;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    public void setQualif(String qualif) {
        this.qualif = qualif;
    }

    public Pilote(Integer matricule, String nom, String prenom, String qualif) {
        this.matricule = matricule;
        this.nom = nom;
        this.prenom = prenom;
        this.qualif = qualif;
    }
    public void ajoutCompagnie(Compagnie compagnie) {
        List<Compagnie> listeCompagnie = new ArrayList();
        listeCompagnie.add(compagnie);
        this.compagnie = compagnie;
    }
}
