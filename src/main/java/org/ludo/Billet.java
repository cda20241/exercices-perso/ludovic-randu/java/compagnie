package org.ludo;

import java.util.ArrayList;
import java.util.List;

import static org.ludo.Fonctions.*;

public class Billet{
    // Déclaration des variables
    private String datePaiement ;
    private String dateReservation ;
    private String dateEmission ;
    private Integer numBillet ;
    private Passager passager ;
    private Vol vol ;
    private Siege siege;



    public Billet() {
        this.dateReservation = nouvelleDate();
        this.datePaiement = nouvelleDate();
        this.dateEmission = nouvelleDate();
        this.numBillet = genNumBillet();
    }
}
