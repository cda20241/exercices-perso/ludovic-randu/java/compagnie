package org.ludo;

import java.util.ArrayList;
import java.util.List;

public class Compagnie {
    private Integer code;
    private String nomCompagnie;
    private String siegeSocial;
    private List<Vol> vols;
    private List<Billet> billets;
    private Pilote pilote;
    private Passager passager;




//GETTER
    public Integer getCode() {
        return code;
    }
    public String getNomCompagnie() {
        return nomCompagnie;
    }
    public String getSiegeSocial() {
        return siegeSocial;
    }
    public Pilote getPilote() {
        return pilote;
    }

    //SETTER
    public void setCode(Integer code) {
        this.code = code;
    }
    public void setNomCompagnie(String nomCompagnie) {
        this.nomCompagnie = nomCompagnie;
    }
    public void setSiegeSocial(String siegeSocial) {
        this.siegeSocial = siegeSocial;
    }

    public void setPilote(Pilote pilote) {
        this.pilote = pilote;
    }

    public Compagnie(Integer code, String nom, String siegeSocial) {
        this.code = code;
        this.nomCompagnie = nom;
        this.siegeSocial = siegeSocial;
        this.vols = new ArrayList<>();
    }
    public void embauche(Pilote employe, Compagnie compagnie){
        List<Pilote> listeEmploye = new ArrayList();
        listeEmploye.add(employe);
        this.pilote = employe;
        List<Compagnie> listeCompagnie = new ArrayList();
        listeCompagnie.add(compagnie);
        this.pilote.ajoutCompagnie(compagnie);

    }

    public Vol creationVol(Integer numVol, String dateDepart, String dateArrivee, String heureDepart, String heureArrivee, String villedepart,
                           String villeArrivee){
        Vol vol =new Vol(numVol, dateDepart, dateArrivee, heureDepart, heureArrivee, villedepart, villeArrivee);
        this.vols.add(vol);
        return vol;
    }
    public Billet achatBillet(Vol vol, Passager passager){
        Billet billet = new Billet();
        this.passager = passager;
        return billet;
    }



}
