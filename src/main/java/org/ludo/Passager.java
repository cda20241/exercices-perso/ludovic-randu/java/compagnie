package org.ludo;


import java.util.ArrayList;
import java.util.List;

public class Passager {
    // Déclaration des variables
    private String nom ;
    private String prenom ;
    private String dateNaissance ;
    private String telephone ;
    private String status ;
    private List<Billet> billets;
    private Vol vol;



    // GETTER
    public String getNom() {
        return nom;
    }
    public String getPrenom() {
        return prenom;
    }
    public String getDateNaissance() {
        return dateNaissance;
    }
    public String getTelephone() {
        return telephone;
    }
    public String getStatus() {
        return status;
    }

    // SETTER
    public void setNom(String nom) {
        this.nom = nom;
    }
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    public void setDateNaissance(String dateNaissance) {
        this.dateNaissance = dateNaissance;
    }
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    // CONSTRUCTEUR
    public Passager(String nom, String prenom, String dateNaissance, String telephone) {
        this.nom = nom;
        this.prenom = prenom;
        this.dateNaissance = dateNaissance;
        this.telephone = telephone;
        this.billets = new ArrayList<>();
    }


    public Billet acheterBillet(){
        Billet billet1 =new Billet();
        this.billets.add(billet1);
        return billet1;
    }


}
